import logo from './logo.svg';
import './App.css';
import {
    Route,
    Link,
    Routes,
    BrowserRouter
} from "react-router-dom";
import HomePage from './Components/HomePage';
import Login from './Components/Login';
import Register from './Components/Register';
import Error404 from './Components/Error404';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <BrowserRouter>
                  <nav>
                      <ul>
                          <li>
                              <Link to={'/HomePage'}>HomePage</Link>
                          </li>
                          <li>
                              <Link to={'/Login'}>Login</Link>
                          </li>
                          <li>
                              <Link to={'/Register'}>Register</Link>
                          </li>
                          <li>
                              <Link to={'/Error404'}>Error404</Link>
                          </li>
                      </ul>
                  </nav>
                  <Routes>
                      <Route path="HomePage" element={<HomePage />} />
                      <Route path="Login" element={<Login />} />
                      <Route path="Register" element={<Register />} />
                      <Route path="Error404" element={<Error404 />} />
                  </Routes>
              </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
